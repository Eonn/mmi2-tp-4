<div id="pages"></div>
</div>
</main>
<footer>
	<div class="wrapper">
		<ul>
			<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-twitter"></i></a></li>
			<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-facebook"></i></a></li>
		</ul>
	</div>
</footer>
</div>

<div class="remodal" data-remodal-id="contact">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Contact</h1>
	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam itaque minima, nesciunt numquam, odio officia quia quo quod recusandae tempore totam voluptatem, voluptatum. Minima molestias nemo nihil perferendis ullam.</p>
	<br>
	<form action="" method="post">
		<div class="form-group"><label for="mail">Email</label><input id="mail" name="mail" type="email" required class="form-control"></div>
		<div class="form-group"><label for="subject">Sujet</label><input id="subject" name="subject" type="text" required class="form-control"></div>
		<div class="form-group"><label for="message">Votre message</label><textarea name="message" id="message" cols="20" rows="5" class="form-control"></textarea></div>
		<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
		<button class="remodal-confirm" type="submit">Envoyer</button>
	</form>
</div>

<script src="https://use.fontawesome.com/601a2e39a6.js"></script>
<script src="js/all.js"></script>

</body>
</html>