<?php include_once('_inc/header.php')  ?>

<article>
	<?php
	if(isset($_GET['recette'])){
		include_once('recettes/'.$_GET['recette'].'.php');
	}else{
	?>
		<h1>Mes recettes favorites</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam illo qui quibusdam sit tempora tempore, vitae! Amet exercitationem laboriosam minus mollitia nisi nulla pariatur, voluptate voluptatum? Architecto atque consectetur ducimus harum reprehenderit? A ad aliquid atque, cupiditate dicta doloremque dolores harum id, incidunt iure labore magnam minima molestias necessitatibus neque nihil obcaecati odit pariatur placeat quos tempore vero vitae voluptate?</p>
		<hr>
		<ul>
			<li><a href="<?= $route['recette'] ?>/filet-de-boeuf-en-croute">Filet de boeuf en croûte</a></li>
			<li><a href="<?= $route['recette'] ?>/pate-a-crepes">Pâte à crêpes</a></li>
			<li><a href="<?= $route['recette'] ?>/tartiflette">Tartiflette</a></li>
		</ul>
	<?php }  ?>
</article>

<?php include_once('_inc/footer.php')  ?>